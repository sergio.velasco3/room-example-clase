package es.estech.roomexample.ui.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import es.estech.roomexample.data.models.Pelicula
import es.estech.roomexample.databinding.HolderListBinding

class MovieAdapter(
    private val listener: MovieClick
) : RecyclerView.Adapter<MovieAdapter.MovieHolder>() {

    interface MovieClick {
        fun onMovieClick (pelicula: Pelicula)
    }

    var lista = ArrayList<Pelicula>()

    inner class MovieHolder(val binding: HolderListBinding) : RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = HolderListBinding.inflate(layoutInflater, parent, false)
        return MovieHolder(binding)
    }

    override fun onBindViewHolder(holder: MovieHolder, position: Int) {
        val movie = lista[position]
        holder.binding.tvTitle.text = movie.name
        holder.binding.tvYear.text = movie.year.toString()

        holder.itemView.setOnClickListener {
            listener.onMovieClick(movie)
        }
    }

    override fun getItemCount(): Int {
        return lista.size
    }

    fun updateUserList(arrayList: List<Pelicula>) {
        lista.clear()
        lista.addAll(arrayList)
        notifyDataSetChanged()
    }
}