package es.estech.roomexample.ui.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import es.estech.roomexample.data.models.Pelicula
import es.estech.roomexample.databinding.FragmentListaBinding
import es.estech.roomexample.ui.adapters.MovieAdapter

class ListFragment : Fragment() {

    private var _binding: FragmentListaBinding? = null
    private val binding get() = _binding!!
    private lateinit var adapter: MovieAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        _binding = FragmentListaBinding.inflate(inflater, container, false)
        return binding.root

    }

    private fun configRecycler() {
        val layoutManager = LinearLayoutManager(requireContext())
        adapter = MovieAdapter(object : MovieAdapter.MovieClick{
            override fun onMovieClick(pelicula: Pelicula) {
                TODO("Click en película")
            }
        })
        binding.recycler.layoutManager = layoutManager
        binding.recycler.adapter = adapter
    }

    private fun addMovie() {
        val name = binding.editTextTextMovie.text.toString()
        val year = binding.editTextYear.text.toString()
        if (name.isNotEmpty() && year.isNotEmpty()) {

            // todo añadir pelicula

        } else Toast.makeText(requireContext(), "Falta información", Toast.LENGTH_SHORT).show()

    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}