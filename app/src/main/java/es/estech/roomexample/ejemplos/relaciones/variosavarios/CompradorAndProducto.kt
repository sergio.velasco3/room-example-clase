package es.estech.roomexample.ejemplos.relaciones.variosavarios

import androidx.room.Entity

@Entity(primaryKeys = ["compradorId", "productoId"])
data class CompradorAndProducto(
    val compradorId: Int,
    val productoId: Int
)
