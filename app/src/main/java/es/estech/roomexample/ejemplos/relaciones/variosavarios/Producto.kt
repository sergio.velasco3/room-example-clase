package es.estech.roomexample.ejemplos.relaciones.variosavarios

import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity
data class Producto(
    @PrimaryKey val productoId: Int,
    val nombre: String,
    val marca: String,
    val categoria: Int,
    val codigo: Int
)
