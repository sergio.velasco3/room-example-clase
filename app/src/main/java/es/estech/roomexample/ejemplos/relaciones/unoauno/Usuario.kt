package es.estech.roomexample.ejemplos.relaciones.unoauno

import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity
data class Usuario(
    @PrimaryKey val userId: Int,
    val name: String,
    val credits: Double
)
