package es.estech.roomexample.ejemplos.relaciones.variosavarios

import androidx.room.Embedded
import androidx.room.Junction
import androidx.room.Relation


data class CompradorConProductos(
    @Embedded val comprador: Comprador,
    @Relation(
        parentColumn = "compradorId",
        entityColumn = "productoId",
        associateBy = Junction(CompradorAndProducto::class)
    )
    val productos: List<Producto>
)
