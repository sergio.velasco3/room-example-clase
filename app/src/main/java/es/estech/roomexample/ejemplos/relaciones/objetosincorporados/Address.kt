package es.estech.roomexample.ejemplos.relaciones.objetosincorporados


data class Address(
    val calle: String,
    val ciudad: String,
    val cp: Int,
    val pais: String
)
