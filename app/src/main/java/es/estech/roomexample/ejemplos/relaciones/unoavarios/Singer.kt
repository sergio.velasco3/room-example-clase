package es.estech.roomexample.ejemplos.relaciones.unoavarios

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Singer(
    @PrimaryKey val singerId: Int,
    val name: String,
    val edad: Int,
    val foundation: Int
)
