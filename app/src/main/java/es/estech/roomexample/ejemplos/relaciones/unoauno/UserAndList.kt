package es.estech.roomexample.ejemplos.relaciones.unoauno

import androidx.room.Embedded
import androidx.room.Relation


data class UserAndList(
    @Embedded val usuario: Usuario,
    @Relation(
        parentColumn = "userId",
        entityColumn = "userOwnerId"
    )
    val playList: PlayList
)