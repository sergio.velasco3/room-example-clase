package es.estech.roomexample.ejemplos.relaciones.variosavarios

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity
data class Comprador(
    @PrimaryKey val compradorId: Int,
    val nombre: String,
    @ColumnInfo(name = "num_tarjeta")
    val tarjeta: Int
)
