package es.estech.roomexample.ejemplos.relaciones.unoavarios

import androidx.room.Embedded
import androidx.room.Relation

data class SingerAndSong(
    @Embedded val singer: Singer,
    @Relation(
        parentColumn = "singerId",
        entityColumn = "singerOwnerId"
    )
    val songList: List<Song>
)
