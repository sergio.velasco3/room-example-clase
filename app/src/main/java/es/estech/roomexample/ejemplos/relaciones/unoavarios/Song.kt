package es.estech.roomexample.ejemplos.relaciones.unoavarios

import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity
data class Song(
    @PrimaryKey
    val id: Int,
    val titulo: String,
    val duracion: String,
    val singerOwnerId: Int
)
