package es.estech.roomexample.ejemplos.basicos.unique

import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey


@Entity(
    indices = [Index(
        value = ["codigoBarras, fechaFabricacion"],
        unique = true
    )]
)
data class SuperProduct(
    @PrimaryKey val id: Int = 0,
    val nombre: String,
    val cantidad: Int,
    val codigoBarras: Int,
    val fechaFabricacion: String
)