package es.estech.roomexample.ejemplos.basicos.autoprimarykey

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity
data class Usuario(

    val name: String,
    val password: String,
    @ColumnInfo(name = "hijos")
    val numHijos: String
){
    @PrimaryKey(autoGenerate = true)
    val id: Int = 0
}
