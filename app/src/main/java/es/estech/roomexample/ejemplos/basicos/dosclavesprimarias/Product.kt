package es.estech.roomexample.ejemplos.basicos.dosclavesprimarias

import androidx.room.Entity


@Entity(primaryKeys = ["name", "brand"])
data class Product(
    val name: String,
    val brand: String
)
