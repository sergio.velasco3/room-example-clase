package es.estech.roomexample.ejemplos.relaciones.objetosincorporados

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import es.estech.roomexample.ejemplos.relaciones.objetosincorporados.Address


@Entity
data class User(
    @PrimaryKey val id: Int,
    val nombre: String,
    val apellidos: String,
    @Embedded val address: Address?
)
