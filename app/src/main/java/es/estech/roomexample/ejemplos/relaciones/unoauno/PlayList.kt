package es.estech.roomexample.ejemplos.relaciones.unoauno

import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity
data class PlayList(
    @PrimaryKey val listId: Int,
    val userOwnerId: Int,
    val listName: String
)
