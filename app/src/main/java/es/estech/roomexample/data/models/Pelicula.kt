package es.estech.roomexample.data.models

data class Pelicula(
    val name: String,
    val year: Int
)